﻿using BEZAO_PayDAL.Encryption;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Interfaces.UnitOfWork;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BEZAO_PayDAL.Application
{
    public static class ServiceOperations
    {
        private static readonly Regex _nameRegex = new Regex(@"^[a-zA-z]+$");
        private static readonly Regex _usernameRegex = new Regex(@"^[a-zA-z\d]+$");
        private static readonly Regex _tenDigitRegex = new Regex(@"^\d{10}$");
        private static readonly Regex _digitRegex = new Regex(@"^[\d]+$");
        private static readonly Regex _emailRegex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
        private static Logger _logger = new Logger();
        private static readonly BezaoPayContext _context = new BezaoPayContext();
        private static readonly IUnitOfWork _unitOfWork = new UnitofWork(_context);
        private static readonly IUserService _userService = new UserService(_unitOfWork);
        private static readonly IAccountService _accountService = new AccountService(_unitOfWork);
        private static readonly ITransactionService _transactionService = new TransactionService(_unitOfWork);
        private static User _user { get; set; }

        public static void End()
        {
            _logger.LogLine("Your session has ended!");
            return;
        }

        public static User Register()
        {
            _logger.LogLine("\nEnter First Name");
            string firstName = Console.ReadLine().Trim();

            while (!_nameRegex.IsMatch(firstName))
            {
                _logger.LogLine("Please enter a valid input!");
                firstName = Console.ReadLine();
            }

            _logger.LogLine("\nEnter Last Name");
            string lastName = Console.ReadLine().Trim();

            while (!_nameRegex.IsMatch(lastName))
            {
                _logger.LogLine("Please enter a valid input!");
                lastName = Console.ReadLine();
            }
            
            _logger.LogLine("\nEnter Username");
            string username = Console.ReadLine().Trim();

            while (!_usernameRegex.IsMatch(username))
            {
                _logger.LogLine("Please enter a valid input!");
                username = Console.ReadLine();
            }

            _logger.LogLine("\nEnter Email");
            string email = Console.ReadLine().Trim();

            while (!_emailRegex.IsMatch(email))
            {
                _logger.LogLine("Please enter a valid email!");
                email = Console.ReadLine();
            }

            _logger.LogLine("\nEnter Date Of Birth");
            var dob = Console.ReadLine().Trim();

            DateTime parsedDOB;
            while (string.IsNullOrWhiteSpace(dob) || !(DateTime.TryParse(dob, out parsedDOB)))
            {
                _logger.LogLine("Please enter a valid date!");
                dob = Console.ReadLine();
            }

            _logger.LogLine("Enter Password");
            string password = Console.ReadLine();

            _logger.LogLine("Confirm Password");
            string confirmPassword = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(password) || password != confirmPassword)
            {
                _logger.LogLine("Passwords do not match\n");

                _logger.LogLine("Enter password");
                password = Console.ReadLine();

                _logger.LogLine("Confirm Password");
                confirmPassword = Console.ReadLine();
            }

            var salt = Hashing.GenerateSalt();

            RegisterViewModel model = new RegisterViewModel
            {
                FirstName = firstName.ToLower(),
                LastName = lastName.ToLower(),
                Username = username.ToLower(),
                Email = email,
                Birthday = parsedDOB,
                Salt = salt,
                Password = Hashing.HashPassword(Encoding.UTF8.GetBytes(confirmPassword), Encoding.UTF8.GetBytes(salt)),
            };

            
            User user = _userService.Register(model);

            if (user != null)
            {
                _user = user;
            }

            return user;
        }

        public static User Login()
        {
            _logger.LogLine("Enter username or email");
            string usernameEmail = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(usernameEmail))
            {
                _logger.LogLine("Please enter a valid input!");
                usernameEmail = Console.ReadLine();
            }

            _logger.LogLine("Enter password");
            string password = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(password))
            {
                _logger.LogLine("Please enter a valid input!");
                password = Console.ReadLine();
            }

            LoginViewModel model = new LoginViewModel
            {
                UsernameEmail = usernameEmail,
                Password = password
            };

            User user = _userService.Login(model);

            if (user != null)
            {
                _user = user;
            }

            return user;
        }

        public static (User, Account) ViewDetails()
        {
            try
            {
                var user = _userService.Get(_user.Id);
                var account = _accountService.Get(_user.Id);
                if (user != null && account != null)
                {
                    _user = user;
                    
                }
                return (user, account);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return (null, null);
            }
        }

        public static void ChangePassword()
        {
            _logger.LogLine("Enter current password");
            string currentPassword = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(currentPassword))
            {
                _logger.LogLine("Please enter a valid input!");
                currentPassword = Console.ReadLine();
            }

            _logger.LogLine("Enter new password");
            string newPassword = Console.ReadLine();

            _logger.LogLine("Confirm Password");
            string confirmPassword = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(newPassword) || newPassword != confirmPassword)
            {
                if (string.IsNullOrWhiteSpace(newPassword))
                {
                    _logger.LogLine("Please enter new password!");
                }

                if (string.IsNullOrWhiteSpace(confirmPassword))
                {
                    _logger.LogLine("Please confirm new password!");

                }

                if (newPassword != confirmPassword)
                {
                    _logger.LogLine("Passwords do not match\n");
                }

                _logger.LogLine("Enter new password");
                newPassword = Console.ReadLine();

                _logger.LogLine("Confirm Password");
                confirmPassword = Console.ReadLine();
            }

            currentPassword = Hashing.HashPassword(Encoding.UTF8.GetBytes(currentPassword), Encoding.UTF8.GetBytes(_user.Salt));

            UpdateViewModel model = new UpdateViewModel 
            {
                Email = _user.Email,
                Username = _user.Username,
                CurrentPassword = currentPassword,
                NewPassword = confirmPassword,
            };

            User user = _userService.UpdatePassword(model);

            if (user != null)
            {
                _user = user;
            }
        }

        public static void ChangeEmail()
        {
            _logger.LogLine("Enter new email");
            string email = Console.ReadLine();

            while (!_emailRegex.IsMatch(email))
            {
                _logger.LogLine("Please enter a valid email!");
                email = Console.ReadLine();
            }

            UpdateViewModel model = new UpdateViewModel { Email = email };
            User user = _userService.UpdateEmail(model);

            if (user != null)
                _user = user;
        }

        public static void Deposit()
        {
            Decimal depositAmount = default;

            _logger.LogLine("Enter Amount");
            string amount = Console.ReadLine();

            while (!_digitRegex.IsMatch(amount))
            {
                _logger.LogLine("Please enter a valid amount!");
                amount = Console.ReadLine();
            }

            try
            {
                depositAmount = Convert.ToDecimal(amount);
            }
            catch (Exception ex)
            {
                _logger.LogLine(ex.Message);
            }

            if (depositAmount == default)
            {
                return;
            }

            DepositViewModel model = new DepositViewModel { AccountId = _user.AccountId, Amount = depositAmount };

            _transactionService.Deposit(model);
        }

        public static decimal? CheckBalance()
        {
            return _accountService.GetBalance(_user.AccountId);
        }

        public static void MakeWithdrawal()
        {
            Decimal withdrawAmount = default;

            _logger.LogLine("Enter Amount");
            string amount = Console.ReadLine();

            while (!_digitRegex.IsMatch(amount))
            {
                _logger.LogLine("Please enter a valid amount!");
                amount = Console.ReadLine();
            }

            try
            {
                withdrawAmount = Convert.ToDecimal(amount);
            }
            catch (Exception ex)
            {
                _logger.LogLine(ex.Message);
            }

            if (withdrawAmount == default)
            {
                return;
            }

            WithdrawalViewModel model = new WithdrawalViewModel { AccountId = _user.AccountId, Amount = withdrawAmount };
             
            _transactionService.Withdraw(model);
        }

        public static void Transfer()
        {
            PromptAccountNumber:
            int accountNumber = default;

            _logger.LogLine("Enter Recipient Account Number");
            string accountNo = Console.ReadLine();

            while (!_digitRegex.IsMatch(accountNo))
            {
                _logger.LogLine("Please enter a valid number!");
                accountNo = Console.ReadLine();
            }

            try
            {
                accountNumber = Convert.ToInt32(accountNo);    
                var recipientAccount = _unitOfWork.Accounts.Find(a => a.AccountNumber == accountNumber).FirstOrDefault();

                if (recipientAccount == null)
                {
                    _logger.LogLine("No Data Found!");
                    goto PromptAccountNumber;
                }

                if (recipientAccount.Id == _user.AccountId)
                {
                    Console.WriteLine("You cannot make a transfer to yourself");
                    goto PromptAccountNumber;
                }

                var recipientName = _unitOfWork.Users.Get(recipientAccount.Id).Name;
                _logger.LogLine($"Account Name: {recipientName}");
            }
            catch (Exception)
            {
                _logger.LogLine("Enter the correct account number!");
                goto PromptAccountNumber;
            }

            Decimal transferAmount = default;

            _logger.LogLine("Enter Amount");
            string amount = Console.ReadLine();

            while (!_digitRegex.IsMatch(amount))
            {
                _logger.LogLine("Please enter a valid amount!");
                amount = Console.ReadLine();
            }

            try
            {
                transferAmount = Convert.ToDecimal(amount);
            }
            catch (Exception ex)
            {
                _logger.LogLine(ex.Message);
            }

            TransferViewModel model = new TransferViewModel
            {
                AccountId = _user.AccountId,
                Amount = transferAmount,
                RecipientAccountNumber = accountNumber
            };

            _transactionService.Transfer(model);
        }
    }
}
