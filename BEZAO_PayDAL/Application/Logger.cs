﻿using BEZAO_PayDAL.Interfaces.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Application
{
    public class Logger : ILogger
    {
        public void Log(string message)
        {
            Console.Write(message);
        }

        public void LogLine(string message)
        {
            Console.WriteLine(message);
        }

        public void LogLine(int message)
        {
            Console.WriteLine(message);
        }
    }
}
