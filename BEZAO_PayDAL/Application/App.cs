﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Application
{
    public static class App
    {
        public static void Run()
        {
            Console.Title = "BEZAO Pay";
            Console.WriteLine("Bezao Pay Services\n");
            Prompt.SelectMenu();
            Prompt.SelectOperation();
        }
    }
}
