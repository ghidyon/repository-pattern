﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Application
{
    public static class Prompt
    {
        private static Logger _logger = new Logger();

        public static void SelectMenu()
        {
            StartMenu:
            Menu();

            string option = Console.ReadLine();
            
            while (string.IsNullOrWhiteSpace(option) || (option != "1" && option != "2"))
            {
                _logger.LogLine("Invalid selection!");
                Menu();
                option = Console.ReadLine();
            }

            if (option == "1")
            {
                var user = ServiceOperations.Register();
                if (user == null)
                    goto StartMenu;
            }
            
            if (option == "2")
            {
                var user = ServiceOperations.Login();
                if (user == null)
                    goto StartMenu;
            }
        }

        public static void SelectOperation()
        {
            OperationMenu:
            OperationMenu();

            string option = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(option) || (option != "1" && option != "2" && option != "3" 
                && option != "4" && option != "5" && option != "6" && option != "7" && option != "8"))
            {
                _logger.LogLine("Invalid selection!");
                OperationMenu();
                option = Console.ReadLine();
            }

            if (option == "1")
            {
                var (user, account) = ServiceOperations.ViewDetails();

                if (user != null && account != null) 
                {
                    _logger.LogLine($"Name: {CapitalizeFirstLetter(user.Name)}");
                    _logger.LogLine($"Username: {user.Username}");
                    _logger.LogLine($"Email: {user.Email}");
                    _logger.LogLine($"Account No: {account.AccountNumber}");
                    _logger.LogLine($"Balance: {account.Balance}");
                }
                goto OperationMenu;
            }

            if (option == "2")
            {
                ServiceOperations.ChangePassword();
                goto OperationMenu;
            }

            if (option == "3")
            {
                ServiceOperations.ChangeEmail();
                goto OperationMenu;
            }

            if (option == "4")
            {
                ServiceOperations.MakeWithdrawal();
                goto OperationMenu;
            }

            if (option == "5")
            {
                ServiceOperations.Transfer();
                goto OperationMenu;
            }

            if (option == "6")
            {
                ServiceOperations.Deposit();
                goto OperationMenu;
            }

            if (option == "7")
            {
                var balance = ServiceOperations.CheckBalance();
                if (balance != null)
                {
                    _logger.LogLine($"Account Bal: N{balance}");
                }
                goto OperationMenu;
            }

            if (option == "8")
            {
                ServiceOperations.End();
            }

        }

        public static void Menu()
        {
            _logger.LogLine("Select an option");
            _logger.LogLine("1. Register");
            _logger.LogLine("2. Login");
        }

        public static void OperationMenu()
        {
            _logger.LogLine("\n");
            _logger.LogLine("Select an option");
            _logger.LogLine("1. View account details");
            _logger.LogLine("2. Change Password");
            _logger.LogLine("3. Change Email Address");
            _logger.LogLine("4. Make a Withdrawal");
            _logger.LogLine("5. Transfer funds");
            _logger.LogLine("6. Fund Account");
            _logger.LogLine("7. Check Balance");
            _logger.LogLine("8. End");
        }

        private static string CapitalizeFirstLetter(string word)
            => char.ToUpper(word[0]) + word.Substring(1).ToLower();
    }
}
