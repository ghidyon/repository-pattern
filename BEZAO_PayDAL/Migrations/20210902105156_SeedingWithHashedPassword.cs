﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BEZAO_PayDAL.Migrations
{
    public partial class SeedingWithHashedPassword : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 9, 2, 13, 51, 52, 348, DateTimeKind.Local).AddTicks(1450), "fbd6RxOVaWIisGcZhfrX0WPXuOcXi7Et0xmvKY5e4Ms=" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 9, 2, 13, 51, 52, 593, DateTimeKind.Local).AddTicks(4929), "x/JZMw932VYg/arGYXHL5SW7zdJgRUzl4EPX5zNZrGg=" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 9, 2, 13, 51, 52, 726, DateTimeKind.Local).AddTicks(2738), "tKtitBXXXgiCmCp7Acoij9G/2KjTCW1Euet0K6IZ/G0=" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 9, 2, 13, 51, 52, 899, DateTimeKind.Local).AddTicks(6521), "QlvNEc/d1+s+++VrJbz0H5plpyRc21URI7ow8SQ0Jyc=" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 9, 2, 13, 51, 53, 312, DateTimeKind.Local).AddTicks(1650), "8QpctCiWBGPVncWK/jp1ofv2Oi/az9u/6VeBgTKTlVI=" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 8, 31, 2, 49, 54, 582, DateTimeKind.Local).AddTicks(6910), "_sorrysir" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 8, 31, 2, 49, 54, 606, DateTimeKind.Local).AddTicks(4046), "_badguy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 8, 31, 2, 49, 54, 606, DateTimeKind.Local).AddTicks(4328), "_dara.sage" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 8, 31, 2, 49, 54, 606, DateTimeKind.Local).AddTicks(4349), "_sadboy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Created", "Password" },
                values: new object[] { new DateTime(2021, 8, 31, 2, 49, 54, 606, DateTimeKind.Local).AddTicks(4366), "_omo" });
        }
    }
}
