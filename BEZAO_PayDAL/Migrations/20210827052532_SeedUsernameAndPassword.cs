﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BEZAO_PayDAL.Migrations
{
    public partial class SeedUsernameAndPassword : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 27, 8, 25, 26, 196, DateTimeKind.Local).AddTicks(785), "_sorrysir", "sorry.sir" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 27, 8, 25, 26, 238, DateTimeKind.Local).AddTicks(2869), "_badguy", "badguy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 27, 8, 25, 26, 238, DateTimeKind.Local).AddTicks(3109), "_dara.sage", "dara.sage" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 27, 8, 25, 26, 238, DateTimeKind.Local).AddTicks(3128), "_sadboy", "sadboy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 27, 8, 25, 26, 238, DateTimeKind.Local).AddTicks(3144), "_omo", "omo" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 26, 12, 56, 45, 352, DateTimeKind.Local).AddTicks(5229), null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 26, 12, 56, 45, 353, DateTimeKind.Local).AddTicks(5419), null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 26, 12, 56, 45, 353, DateTimeKind.Local).AddTicks(5463), null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 26, 12, 56, 45, 353, DateTimeKind.Local).AddTicks(5466), null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Created", "Password", "Username" },
                values: new object[] { new DateTime(2021, 8, 26, 12, 56, 45, 353, DateTimeKind.Local).AddTicks(5468), null, null });
        }
    }
}
