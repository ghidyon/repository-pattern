﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BEZAO_PayDAL.Encryption
{
    internal static class Hashing
    {
        public static string GenerateSalt()
        {
            byte[] salt = new byte[128 / 8];

            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetNonZeroBytes(salt);
            }
            return Convert.ToBase64String(salt);
        }

        public static string HashPassword(byte[] password, byte[] salt)
        {
            var hashKey = new Rfc2898DeriveBytes(password, salt, 10_000);
            var hash = hashKey.GetBytes(32);
            return Convert.ToBase64String(hash);
        }
    }
}
