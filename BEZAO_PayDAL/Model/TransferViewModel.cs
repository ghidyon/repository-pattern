﻿namespace BEZAO_PayDAL.Model
{
    public class TransferViewModel
    {
        public int AccountId { get; set; }
        public int RecipientAccountNumber { get; set; }
        public decimal Amount { get; set; }
    }
}