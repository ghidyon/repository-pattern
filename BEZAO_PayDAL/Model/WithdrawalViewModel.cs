﻿namespace BEZAO_PayDAL.Model
{
    public class WithdrawalViewModel
    {
        public int AccountId { get; set; }
        public decimal Amount { get; set; }

    }
}