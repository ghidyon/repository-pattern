﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Interfaces.Helpers
{
    interface ILogger
    {
        void Log(string message);
        void LogLine(string message);
        void LogLine(int message);
    }
}
