﻿using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Model;

namespace BEZAO_PayDAL.Interfaces.Services
{
    public  interface IUserService
    {
        User Register(RegisterViewModel model);
        User UpdateEmail(UpdateViewModel model);
        User UpdatePassword(UpdateViewModel model);
        User Login(LoginViewModel model);
        void Delete(int id);
        User Get(int id);

    }
}
