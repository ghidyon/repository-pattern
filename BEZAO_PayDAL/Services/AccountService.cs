﻿using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Account Get(int accountId)
        {
            return _unitOfWork.Accounts.Get(accountId);
        }

        public decimal? GetBalance(int accountId)
        {
            var account = Get(accountId);

            if (account == null)
            {
                Console.WriteLine("Account not found!");
                return null;
            }

            return account.Balance;
        }
    }
}
