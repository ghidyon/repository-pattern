﻿using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BEZAO_PayDAL.Interfaces.UnitOfWork;

namespace BEZAO_PayDAL.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransactionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Deposit(DepositViewModel model)
        {
            var account = _unitOfWork.Accounts.Find(a => a.Id == model.AccountId).FirstOrDefault();

            if(account == null)
            {
                Console.WriteLine("Account not found!");
                return;
            }

            if (model.Amount <= 0)
            {
                Console.WriteLine("Invalid Amount");
                return;
            }

            try
            {
                account.Balance += model.Amount;
                Transaction transaction = new Transaction {
                    UserId = account.Id,
                    TransactionMode = TransactionMode.Credit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now 
                };

                _unitOfWork.Accounts.Update(account);

                _unitOfWork.Transactions.Add(transaction);
                _unitOfWork.Commit();
                Console.WriteLine("Your deposit is successful");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Something went wrong, Try Again!");
            }
            return;
        }

        public void Withdraw(WithdrawalViewModel model)
        {
            var account = _unitOfWork.Accounts.Find(a => a.Id == model.AccountId).FirstOrDefault();
            
            if (account == null)
            {
                Console.WriteLine("Account not found!");
                return;
            }

            if (model.Amount < 0)
            {
                Console.WriteLine("Invalid Amount");
                return;
            }

            if (account.Balance < model.Amount)
            {
                Console.WriteLine("Insufficient Funds!");
                return;
            }

            try
            {
                account.Balance -= model.Amount;
                Transaction transaction = new Transaction
                {
                    UserId = account.Id,
                    TransactionMode = TransactionMode.Debit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now
                };


                _unitOfWork.Accounts.Update(account);
                _unitOfWork.Transactions.Add(transaction);
                _unitOfWork.Commit();
                Console.WriteLine($"Withdrawal of N{model.Amount}, successfully!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Transfer(TransferViewModel model)
        {
            var senderAccount = _unitOfWork.Accounts.Find(a => a.Id == model.AccountId).FirstOrDefault();
            var recipientAccount = _unitOfWork.Accounts.Find(a => a.AccountNumber == model.RecipientAccountNumber).FirstOrDefault();


            if (senderAccount == null || recipientAccount == null)
            {
                Console.WriteLine("Account not found!");
                return;
            }

            if (model.Amount <= 0)
            {
                Console.WriteLine("Invalid Amount");
                return;
            }

            if (senderAccount.Balance < model.Amount)
            {
                Console.WriteLine("Insufficient Funds!");
                return;
            }

            try
            {
                senderAccount.Balance -= model.Amount;
            
                recipientAccount.Balance += model.Amount;
            
                Transaction recipientTransaction = new Transaction
                {
                    UserId = recipientAccount.Id,
                    TransactionMode = TransactionMode.Credit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now
                };

                Transaction senderTransaction = new Transaction
                {
                    UserId = senderAccount.Id,
                    TransactionMode = TransactionMode.Credit,
                    Amount = model.Amount,
                    TimeStamp = DateTime.Now
                };

                List<Transaction> transactions = new List<Transaction>();
                transactions.Add(recipientTransaction);
                transactions.Add(senderTransaction);

                _unitOfWork.Accounts.Update(senderAccount);
                _unitOfWork.Accounts.Update(recipientAccount);
                _unitOfWork.Transactions.AddRange(transactions);
                _unitOfWork.Commit();
                Console.WriteLine($"Transfered N{model.Amount:n} to {senderAccount.AccountNumber}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Something went wrong. Pls Try Again");
            }
        }

    }
}
