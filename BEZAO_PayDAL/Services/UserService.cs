﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Interfaces.UnitOfWork;
using BEZAO_PayDAL.Model;
using System.Security.Cryptography;
using BEZAO_PayDAL.Encryption;

namespace BEZAO_PayDAL.Services
{
  public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public User Register(RegisterViewModel model)
        {
            var user = new User
            {
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Username = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Password = model.Password,
                Salt = model.Salt,
                Account = new Account{ AccountNumber = RandomNumberGenerator.GetInt32(0000000001, 2147483647) },
                Created = DateTime.Now
            };

            try
            {
                _unitOfWork.Users.Add(user);
                Console.WriteLine("Saving...");
                _unitOfWork.Commit();
                Console.WriteLine("Success!");

                var userData = _unitOfWork.Users.Find(u => u.IsActive == true && u.Username == user.Username).FirstOrDefault();
                return userData;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public User Login(LoginViewModel model)
        {
            var user = _unitOfWork.Users.Find(u => u.Username == model.UsernameEmail || u.Email == model.UsernameEmail).FirstOrDefault();

            if(user == null)
            {
                Console.WriteLine("User does not exist!");
                return null;
            }

            string password = Hashing.HashPassword(Encoding.UTF8.GetBytes(model.Password), Encoding.UTF8.GetBytes(user.Salt));

            var isValidPassword = IsValidPassword(user.Password, password);

            if(!isValidPassword)
            {
                Console.WriteLine("Incorrect Password!");
                return null;
            }

            Console.WriteLine($"Logged in as \nName: {user.Name}");
            return user;
        }
        
        public User UpdatePassword(UpdateViewModel model)
        {

            var user = _unitOfWork.Users.Find(u => u.Username == model.Username).FirstOrDefault();
            
            if (user == null)
            {
                Console.WriteLine("user not found");
                return null;
            }

            if (!IsValidPassword(model.CurrentPassword, user.Password))
            {
                Console.WriteLine("Incorrect Password");
                return null;
            }

            try
            {
                var salt = user.Salt;
                user.Password = Hashing.HashPassword(Encoding.UTF8.GetBytes(model.NewPassword), Encoding.UTF8.GetBytes(salt));

                _unitOfWork.Users.Update(user);
                _unitOfWork.Commit();
                Console.WriteLine("Successfully change password");

                return _unitOfWork.Users.Find(u => u.Username == model.Username).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            
        }

        public User UpdateEmail(UpdateViewModel model)
        {
            var user = _unitOfWork.Users.Find(u => u.Username == model.Username).FirstOrDefault();

            if (user == null)
            {
                Console.WriteLine("user not found");
                return null;
            }

            try
            {
                user.Email = model.Email;
                _unitOfWork.Users.Update(user);
                _unitOfWork.Commit();
                Console.WriteLine("Updated Email");

                return _unitOfWork.Users.Find(u => u.Username == model.Username).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public void Delete(int id)
        {
            var user = Get(id);
            if(user == null)
            {
                Console.WriteLine("User does not exist");
                return;
            }

            user.IsActive = false;
            _unitOfWork.Users.Update(user);
            _unitOfWork.Commit();
        }

        public User Get(int id)
        {
            return _unitOfWork.Users.Get(id);
        }

        public bool IsValidPassword(string userPassword, string password)
        {
            return (userPassword == password);
        }
    }
}
