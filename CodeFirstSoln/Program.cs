﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using BEZAO_PayDAL.Application;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstSoln
{
    partial class Program
    {
        static void Main(string[] args)
        {
            App.Run();
        }
    }
}
